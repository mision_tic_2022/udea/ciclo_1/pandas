
import pandas as pd
import numpy as np

guion = lambda : print('--------------------------------------------------\n')

#Leer un archivo desde pandas
casos_codiv = pd.read_csv('casos_covid_19.csv')

#Obtener la información del dataframe
print( casos_codiv.info() )
guion()
guion()
print( casos_codiv.describe() )

guion()
guion()
print( casos_codiv.head() )
guion()
print( casos_codiv.tail() )

#Sobreescribir valores nulos
casos_codiv['País de procedencia'].fillna('Colombia', inplace=True)
print( casos_codiv.head() )

guion()
guion()
casos_codiv['Sexo'].fillna('NULL', inplace=True)

#personas_masculinas = casos_codiv[ casos_codiv['Sexo'] == 'M' ]
#print( personas_masculinas.describe() )

procedencia_españa = np.array( list( filter(lambda pais: pais.lower()=='españa',  casos_codiv['País de procedencia']) ) )
print('Personas de españa: ', len(procedencia_españa))