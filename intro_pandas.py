
from operator import index
import pandas as pd
import numpy as np

guion = lambda : print('--------------------------------------------------')

ventas = pd.Series( [10, 20, 30, 40, 50] )

print(ventas)

guion()
#Acceder al Objeto que contiene el indice de la serie
print( ventas.index )

guion()
#Sobreescribir objeto index
ventas.index = ['ene','feb','mar', 'abr','may']
print(ventas.index)

guion()
#Obtener el tipo de dato de la serie
print( ventas.dtype )

guion()
#Acceder a un valor
print( ventas[1] )
print( ventas['feb'] )

guion()
print(ventas)
ventas.index.name = 'Meses'
ventas.name = 'Ventas 2022'
print( ventas )


guion()
#Consultar/obtener las dimensiones
print( ventas.axes )
guion()
#Consultar tamaño de la serie
print( ventas.shape )

guion()
print('DATAFRAME')
guion()

ventas_frutas = {
    'peras': [10, 20, 80, 90],
    'manzanas': [60, 80, 50, 20]
}

data_frutas = pd.DataFrame( ventas_frutas, index=['ene','feb','mar','abr'] )
print(data_frutas)

guion()
index_filas = data_frutas.index
index_columnas = data_frutas.columns
print(index_filas)
print(index_columnas)

#CREAR DATAFRAME A PARTIR DE SERIES
guion()

entradas = pd.Series( [10, 20, 80, 90,100, 200, 150, 850, 450, 280, 111, 112], 
                        index=['ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic'] )
salidas = pd.Series( [15, 8, 80, 60,150, 120, 50, 50, 50, 300, 260, 460], 
                        index=['ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic'] )

dict_almacen = {
    'entradas': entradas,
    'salidas': salidas
}

#crear dataframe
almacen = pd.DataFrame( dict_almacen )
print(almacen)

#Crear nueva columna
guion()
almacen['neto'] = almacen.entradas - almacen.salidas
print(almacen)

guion()
#Selección de una columna
print( almacen.entradas )
guion()
print( almacen['entradas'] )

guion()
print( almacen.neto )

guion()
#Consultar datos e información de un dataframe
#acceder a los primeros cinco registros
print(almacen.head())
#acceder a los primeros 10 registros
print(almacen.head(10))

guion()
#acceder a los último 5 registros
print(almacen.tail())

guion()
#acceder a una fila de manera aleatoria
fila_aleatoria = almacen.sample()
print( fila_aleatoria )

#acceder a una cantidad específica de filas de manera aleatoria
filas = almacen.sample(5)
print( filas )

guion()
print( almacen.describe() )

guion()
#Seleccionar datos de una serie
serie = pd.Series( [10,8,20,25,15] )
seleccion = [ True, False, True, False, True ]

print( serie[seleccion] )
guion()
print( serie[ serie > 15 ] )

#SELECCIÓN DE DATOS CON DATAFRAME
guion()
registros_negativos = almacen[ almacen.neto < 0 ]
print(registros_negativos)

guion()
#OBTENER REGISTRO MENOR DE SALIDAS DEL ALMACEN
minimo = almacen.min()
print(minimo)
guion()
#MÁXIMO
maximo = almacen.max()
print(maximo)
print( maximo['entradas'] )